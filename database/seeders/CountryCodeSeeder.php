<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountryCodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
    $countries = [
    [
        'name' => 'Maldives',
        'code' => '960'
    ],
        [
            'name' => 'India',
            'code' => '91'
        ]
    ];

    foreach ($countries as $country) {
        DB::table('country_codes')->insert([
            'name' => $country['name'],
            'code' => $country['code'],
        ]);
    }
    }
}
